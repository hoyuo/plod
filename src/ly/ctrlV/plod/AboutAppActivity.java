package ly.ctrlV.plod;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;

public class AboutAppActivity extends Activity {
	//어플 설명
	private ViewPager mPager;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_about_app);

	

		mPager = (ViewPager)findViewById(R.id.pager);
		mPager.setAdapter(new PagerAdapterClass(getApplicationContext()));
	}


	private void setCurrentInflateItem(int type){
		if(type==0){
			mPager.setCurrentItem(0);
		}else if(type==1){
			mPager.setCurrentItem(1);
		}else{
			mPager.setCurrentItem(2);
		}
	}

	
	 /**
	  * PagerAdapter 
	  */
	  private class PagerAdapterClass extends PagerAdapter{

		  private LayoutInflater mInflater;

		  public PagerAdapterClass(Context c){
			  super();
			  mInflater = LayoutInflater.from(c);
		  }

		  @Override
		  public int getCount() {
			  return 3;
		  }

		  @Override
		  public Object instantiateItem(View pager, int position) {
			  View v = null;
			  if(position==0){
				  v = mInflater.inflate(R.layout.inflate_one, null);
				 
			  }
			  else if(position==1){
				  v = mInflater.inflate(R.layout.inflate_two, null);
				 
			  }else{
				  v = mInflater.inflate(R.layout.inflate_three, null);
				
			  }

			  ((ViewPager)pager).addView(v, 0);

			  return v; 
		  }

		  @Override
		  public void destroyItem(View pager, int position, Object view) {    
			  ((ViewPager)pager).removeView((View)view);
		  }

		  @Override
		  public boolean isViewFromObject(View pager, Object obj) {
			  return pager == obj; 
		  }

		  @Override public void restoreState(Parcelable arg0, ClassLoader arg1) {}
		  @Override public Parcelable saveState() { return null; }
		  @Override public void startUpdate(View arg0) {}
		  @Override public void finishUpdate(View arg0) {}
	  }

}
