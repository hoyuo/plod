package ly.ctrlV.plod;

import java.util.ArrayList;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapActivity extends Fragment implements
		GoogleMap.OnMarkerClickListener {

	ArrayList<YouTubeSoruce> youtube;
	ArrayList<Marker> markers;
	MapView mMapView;
	LocationManager locationManager;
	String locationProvider;
	private GoogleMap googleMap = null; // Might be null if Google Play services
										// APK
	// is not available.
	private static final String ARG_SECTION_NUMBER = "section_number";

	public static Fragment newInstance(int sectionNumber) {
		MapActivity fragment = new MapActivity();
		Bundle args = new Bundle();
		args.putInt(ARG_SECTION_NUMBER, sectionNumber);
		fragment.setArguments(args);
		return fragment;
	}

	public MapActivity() {

	}

	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.activity_map, container,
				false);
		mMapView = (MapView) rootView.findViewById(R.id.map);
		mMapView.onCreate(savedInstanceState);
		mMapView.onResume();// needed to get the map to display immediately
		try {
			MapsInitializer.initialize(getActivity().getApplicationContext());
		} catch (Exception e) {
			e.printStackTrace();
		}
		googleMap = mMapView.getMap();
		// StartLocationService(getActivity().getApplicationContext());
		// latitude and longitude
		AddMarkers();

		// Perform any camera updates here
		return rootView;
	}

	private void StartLocationService(Context ctx) {
		// 위치 관리자 객체 참조
		LocationManager manager = (LocationManager) ctx
				.getSystemService(Context.LOCATION_SERVICE);

		// 리스너 객체 생성
		GpsLocation gpsLocation = new GpsLocation();
		long minTime = 1000;
		float minDistance = 0;

		// GPS 기반 위치 요청
		manager.requestLocationUpdates(LocationManager.GPS_PROVIDER, minTime,
				minDistance, gpsLocation);

		// 네트워크 기반 위치 요청
		manager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,
				minTime, minDistance, gpsLocation);

		locationManager = (LocationManager) ctx
				.getSystemService(Context.LOCATION_SERVICE);

		locationProvider = locationManager
				.getBestProvider(new Criteria(), true);

		Toast.makeText(ctx, locationProvider, Toast.LENGTH_SHORT).show();

		// CameraLocation(location.getLatitude(), location.getLongitude());
		// String msg = location.getLatitude() + " : " +
		// location.getLongitude();

	}

	private void showCurrentLocation(Double latitude, Double longitude) {
		// 현재 위치를 이용해 LatLon 객체 생성
		LatLng curPoint = new LatLng(latitude, longitude);

		googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(curPoint, 16f));
	}

	private class GpsLocation implements LocationListener {
		@Override
		public void onLocationChanged(Location location) {
			// Double latitude = location.getLatitude();
			// Double longitude = location.getLongitude();
			Double latitude = location.getLatitude();
			Double longitude = location.getLongitude();
			showCurrentLocation(latitude, longitude);
		}

		@Override
		public void onStatusChanged(String provider, int status, Bundle extras) {

		}

		@Override
		public void onProviderEnabled(String provider) {

		}

		@Override
		public void onProviderDisabled(String provider) {

		}
	}

	private void AddMarkers() {
		ProcessYouTube processYouTube = new ProcessYouTube();
		youtube = processYouTube.XmlPassing();
		markers = new ArrayList<Marker>();
		youtube.add(new YouTubeSoruce(0, "광화문", 37.571819, 126.976425,
				"U0ONK7L1fFg"));
		

		for (int i = 0; i < youtube.size(); i++) {
			double latitude = youtube.get(i).latitude;
			double longitude = youtube.get(i).longtitude;
			String subject = youtube.get(i).subject;

			MarkerOptions marker = new MarkerOptions().position(
					new LatLng(latitude, longitude)).title(subject);

			marker.icon(BitmapDescriptorFactory
					.fromResource(R.drawable.markpin));
			// adding marker
			Marker temp = googleMap.addMarker(marker);
			markers.add(temp);
		}

		googleMap.setOnMarkerClickListener(this);
	}

	private void CameraLocation(Double latitude, Double longitude) {
		CameraPosition cameraPosition = new CameraPosition.Builder()
				.target(new LatLng(latitude, longitude)).zoom(14).build();
		googleMap.animateCamera(CameraUpdateFactory
				.newCameraPosition(cameraPosition));

	}

	@Override
	public void onResume() {
		super.onResume();
		googleMap.setMyLocationEnabled(true);
		mMapView.onResume();
	}

	@Override
	public void onPause() {
		super.onPause();
		googleMap.setMyLocationEnabled(false);
		mMapView.onPause();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		mMapView.onDestroy();
	}

	@Override
	public void onLowMemory() {
		super.onLowMemory();
		mMapView.onLowMemory();
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		((MainActivity) activity).onSectionAttached(getArguments().getInt(
				ARG_SECTION_NUMBER));
	}

	@Override
	public boolean onMarkerClick(Marker arg0) {
		// TODO Auto-generated method stub
		int index = markers.indexOf(arg0);

		if (index != -1) {
			Intent palyer = new Intent();
			palyer.setClassName("ly.ctrlV.plod", "ly.ctrlV.plod.PlayerActivity");
			Log.i("Te", youtube.get(index).url);
			palyer.putExtra("URL", youtube.get(index).url);
			palyer.putExtra("SUBJECT", youtube.get(index).subject);
			startActivityForResult(palyer, 0);
		}

		return false;
	}

}