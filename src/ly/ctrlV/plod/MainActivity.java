package ly.ctrlV.plod;

import android.app.ActionBar;
import android.app.Activity;
import android.app.FragmentManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;

public class MainActivity extends Activity implements
		NavigationDrawerFragment.NavigationDrawerCallbacks {

	/**
	 * Fragment managing the behaviors, interactions and presentation of the
	 * navigation drawer.
	 */
	private NavigationDrawerFragment mNavigationDrawerFragment;

	/**
	 * Used to store the last screen title. For use in
	 * {@link #restoreActionBar()}.
	 */
	private CharSequence mTitle;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		mNavigationDrawerFragment = (NavigationDrawerFragment) getFragmentManager()
				.findFragmentById(R.id.navigation_drawer);
		mTitle = getTitle();

		// Set up the drawer.
		mNavigationDrawerFragment.setUp(R.id.navigation_drawer,
				(DrawerLayout) findViewById(R.id.drawer_layout));

		restoreActionBar();
	}

	@Override
	public void onNavigationDrawerItemSelected(int position) {
		// update the main content by replacing fragments
		FragmentManager fragmentManager = getFragmentManager();
		fragmentManager.beginTransaction().replace(R.id.container
		// ,PlaceholderFragment.newInstance(position + 1)).commit();
				, MapActivity.newInstance(position + 1)).commit();
	}

	public void onSectionAttached(int number) {
		switch (number) {
		case 1: {
			mTitle = getString(R.string.title_section1);
			// FragmentManager fragmentManager = getFragmentManager();
			// fragmentManager
			// .beginTransaction()
			// .replace(R.id.container, MapActivity.newInstance()).commit();
		}
			break;
		case 2:
			mTitle = getString(R.string.title_section2);
			Intent notice = new Intent(this, NoticeActivity.class);
			startActivity(notice);
			break;
		case 3:
			mTitle = getString(R.string.title_section3);
			Intent aboutapp = new Intent(this, AboutAppActivity.class);
			startActivity(aboutapp);
			break;
		case 4:
			mTitle = getString(R.string.title_section4);
			Intent help = new Intent(this, HelpActivity.class);
			startActivity(help);
			// FragmentManager fragmentManager = getFragmentManager();
			// fragmentManager
			// .beginTransaction()
			// .replace(R.id.container,
			// HelpActivity.newInstance()).commit();
			break;
		}
	}

	public void restoreActionBar() {
		ActionBar actionBar = getActionBar();
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
		actionBar.setDisplayShowTitleEnabled(true);
		actionBar.setDisplayUseLogoEnabled(false);
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.setDisplayShowHomeEnabled(false);
	}
}
