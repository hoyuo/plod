package ly.ctrlV.plod;

import java.util.ArrayList;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.TextView;

public class NoticeActivity extends Activity {
	// 공지사항
	public ArrayList<Notice> notice;
	private ArrayList<String> group;
	private ArrayList<String> child_Data;
	private ArrayList<ArrayList<String>> child;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_notice);

		loadData();

		ExpandableListView l = (ExpandableListView) findViewById(R.id.ExpandListView1);
		l.setGroupIndicator(null);

		myExpandalbeAdapter adapter = new myExpandalbeAdapter(this, group,
				child, child_Data);
		l.setAdapter(adapter);

	}

	public class myExpandalbeAdapter extends BaseExpandableListAdapter {
		private ArrayList<String> group;
		private ArrayList<ArrayList<String>> child;
		private ArrayList<String> child_Data;
		private Context context;

		public myExpandalbeAdapter(Context context, ArrayList<String> group,
				ArrayList<ArrayList<String>> child, ArrayList<String> child_Data) {
			this.context = context;
			this.group = group;
			this.child = child;
			this.child_Data = child_Data;
		}

		public boolean areAllItemsEnabled() {
			return true;
		}

		@Override
		public String getChild(int groupPosition, int childPosition) {
			// TODO Auto-generated method stub
			return child.get(groupPosition).get(childPosition);
		}

		@Override
		public long getChildId(int groupPosition, int childPosition) {
			// TODO Auto-generated method stub
			return childPosition;
		}

		@Override
		public View getChildView(int groupPosition, int childPosition,
				boolean isLastChild, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			String ch = getChild(groupPosition, childPosition);
			String ch2 = getChild(groupPosition, childPosition + 1);
			if (convertView == null) {
				LayoutInflater inflater = (LayoutInflater) context
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				convertView = inflater.inflate(
						R.layout.expandablelistview_child, null);
			}
			TextView childText = (TextView) convertView
					.findViewById(R.id.textview1);
			TextView childText2 = (TextView) convertView
					.findViewById(R.id.textview2);
			childText.setText(ch2);
			childText2.setText(ch);

			return convertView;
		}

		@Override
		public int getChildrenCount(int groupPosition) {
			// TODO Auto-generated method stub
			return child.get(groupPosition).size() / 2;
		}

		@Override
		public String getGroup(int groupPosition) {
			// TODO Auto-generated method stub
			return group.get(groupPosition);
		}

		@Override
		public int getGroupCount() {
			// TODO Auto-generated method stub
			return group.size();
		}

		@Override
		public long getGroupId(int groupPosition) {
			// TODO Auto-generated method stub
			return groupPosition;
		}

		@Override
		public View getGroupView(int groupPosition, boolean isExpanded,
				View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			String group = getGroup(groupPosition);

			if (convertView == null) {
				LayoutInflater inflater = (LayoutInflater) context
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				convertView = inflater.inflate(
						R.layout.expandalbelistview_group, null);
			}
			TextView groupText = (TextView) convertView
					.findViewById(R.id.TextViewGroup);
			groupText.setText(group);
			return convertView;
		}

		@Override
		public boolean hasStableIds() {
			// TODO Auto-generated method stub
			return true;
		}

		@Override
		public boolean isChildSelectable(int arg0, int arg1) {
			// TODO Auto-generated method stub
			return true;
		}

	}

	private void loadData() {
		NoticeTask nt = new NoticeTask();
		notice = nt.XmlPassing();

		group = new ArrayList<String>();
		child = new ArrayList<ArrayList<String>>();
		for (int i = 0; i < notice.size(); i++) {
			group.add(notice.get(i).title);

			child_Data = new ArrayList<String>();
			child_Data.add(notice.get(i).date);
			child_Data.add("\t\t\t" + notice.get(i).context);
			child.add(child_Data);
			child_Data = null;

		}
	}
}