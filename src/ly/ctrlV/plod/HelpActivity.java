package ly.ctrlV.plod;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class HelpActivity extends Activity {
//도움말
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_help);
		getActionBar().setDisplayHomeAsUpEnabled(true);
		
		Button emailbtn = (Button)findViewById(R.id.emailbutton);
		emailbtn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				 Uri uri=Uri.parse("mailto:plodvideos@gmail.com");
		         Intent i = new Intent(Intent.ACTION_SENDTO,uri);
		         startActivity(i);
			}
		});
		
	}
	
}

//public class HelpActivity extends Fragment {
//
//	/**
//	 * Returns a new instance of this fragment for the given section number.
//	 */
//	public static HelpActivity newInstance() {
//		HelpActivity fragment = new HelpActivity();
//		// Bundle args = new Bundle();
//		// args.putInt(ARG_SECTION_NUMBER, sectionNumber);
//		// fragment.setArguments(args);
//		return fragment;
//	}
//
//	@Override
//	public void onStart() {
//		// TODO Auto-generated method stub
//		super.onStart();
//		Button emailbtn = (Button)getView().findViewById(R.id.emailbutton);
//		emailbtn.setOnClickListener(new OnClickListener() {
//			
//			@Override
//			public void onClick(View v) {
//				// TODO Auto-generated method stub
//				 Uri uri=Uri.parse("mailto:plodvideos@gmail.com");
//		         Intent i = new Intent(Intent.ACTION_SENDTO,uri);
//		         startActivity(i);
//			}
//		});
//	}
//
//	public HelpActivity() {
//	}
//
//	@Override
//	public View onCreateView(LayoutInflater inflater, ViewGroup container,
//			Bundle savedInstanceState) {
//		View rootView = inflater.inflate(R.layout.activity_help, container,
//				false);
//
//		return rootView;
//	}
//
//}