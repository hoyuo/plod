package ly.ctrlV.plod;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;

public class StartAboutActivity extends Activity {
//시작 -> 어플 설명
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_start_about);
		
		ImageButton imgBtn = (ImageButton)findViewById(R.id.startaboutbutton);
		
		imgBtn.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				// TODO Auto-generated method stub

				Intent i = new Intent(StartAboutActivity.this, MainActivity.class);
				startActivity(i);

				finish();
			}
		});	
	}
}
