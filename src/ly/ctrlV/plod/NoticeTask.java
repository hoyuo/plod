package ly.ctrlV.plod;

import java.net.URL;
import java.util.ArrayList;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import android.os.StrictMode;
import android.util.Log;

public class NoticeTask {

	ArrayList<Notice> notice = null;

	public ArrayList<Notice> XmlPassing() {
		try {
			StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
					.permitAll().build();
			StrictMode.setThreadPolicy(policy);

			notice = new ArrayList<Notice>();
			Notice temp = null;

			URL url = new URL("http://hoyuo.me/Notice.php");
			XmlPullParserFactory parserFactory = XmlPullParserFactory
					.newInstance();
			XmlPullParser parser = parserFactory.newPullParser();
			parser.setInput(url.openStream(), "utf-8");

			int parserEvent = parser.getEventType();
			Log.i("te", "�Ľ�");
			while (parserEvent != XmlPullParser.END_DOCUMENT) {
				switch (parserEvent) {
				case XmlPullParser.START_DOCUMENT:
					break;
				case XmlPullParser.START_TAG:
					String name = parser.getName();
					if (name != null && name.equals("node")) {
						String idNotice = parser.getAttributeValue(null,
								"idNotice");
						String title = parser.getAttributeValue(null, "title");
						String context = parser.getAttributeValue(null,
								"context");
						String date = parser.getAttributeValue(null, "date");
						temp = new Notice(idNotice, title, context, date);
					}
					break;

				case XmlPullParser.TEXT:

					break;

				case XmlPullParser.END_TAG:
					if (temp != null) {
						notice.add(temp);
					}
					temp = null;
				}

				parserEvent = parser.next();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return notice;
	}

}
