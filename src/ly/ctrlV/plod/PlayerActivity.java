package ly.ctrlV.plod;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;

public class PlayerActivity extends YouTubeFailureRecoveryActivity {
	// 재생화면
	String url = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_player);
		YouTubePlayerView youTubeView = (YouTubePlayerView) findViewById(R.id.youtube_view);
		youTubeView.initialize(DeveloperKey.DEVELOPER_KEY, this);
		Intent intent = getIntent();
		url = intent.getStringExtra("URL");
		ProcessYouTube py = new ProcessYouTube();
		String title = py.getData(url);
		if(title == ""){
			title = intent.getStringExtra("SUBJECT");
		}
		TextView titleView = (TextView)findViewById(R.id.title);
		titleView.setText(title);
	}

	public void onInitializationSuccess(YouTubePlayer.Provider provider,
			YouTubePlayer player, boolean wasRestored) {
		if (url == null) {
			finish();
		} else if (!wasRestored) {
			player.cueVideo(url);
		}
	}

	protected YouTubePlayer.Provider getYouTubePlayerProvider() {
		return (YouTubePlayerView) findViewById(R.id.youtube_view);
	}
}


