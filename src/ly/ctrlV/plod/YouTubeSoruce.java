package ly.ctrlV.plod;

public class YouTubeSoruce {
	public int idx;
	public String subject;
	public double latitude;
	public double longtitude;
	public String url;

	public YouTubeSoruce(int idx, String subject, double latitude,
			double longtitude, String url) {
		super();
		this.idx = idx;
		this.subject = subject;
		this.latitude = latitude;
		this.longtitude = longtitude;
		this.url = url;
	}
}
