package ly.ctrlV.plod;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

public class PreviousActivity extends Activity {
//스플래쉬 화면
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_previous);
		
		Handler mHandler = new Handler();
		mHandler.postDelayed(new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				Intent i = new Intent(PreviousActivity.this, StartAboutActivity.class);
				
				startActivity(i);
				finish();
			}
		}, 1500);
	}
}
