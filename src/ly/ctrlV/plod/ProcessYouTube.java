package ly.ctrlV.plod;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;

import net.htmlparser.jericho.Element;
import net.htmlparser.jericho.HTMLElementName;
import net.htmlparser.jericho.Source;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import android.os.StrictMode;

class ProcessYouTube {

	ArrayList<YouTubeSoruce> p = null;
	
	
	public ArrayList<YouTubeSoruce> XmlPassing() {
		try {
			StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
					.permitAll().build();
			StrictMode.setThreadPolicy(policy);

			p = new ArrayList<YouTubeSoruce>();
			YouTubeSoruce temp = null;

			URL url = new URL("http://hoyuo.me/YouTube.php");
			XmlPullParserFactory parserFactory = XmlPullParserFactory
					.newInstance();
			XmlPullParser parser = parserFactory.newPullParser();
			parser.setInput(url.openStream(), "utf-8");

			int parserEvent = parser.getEventType();

			while (parserEvent != XmlPullParser.END_DOCUMENT) {
				switch (parserEvent) {
				case XmlPullParser.START_DOCUMENT:
					break;
				case XmlPullParser.START_TAG:
					String name = parser.getName();
					if (name != null && name.equals("node")) {
						temp = null;

						String idx = parser.getAttributeValue(null, "IDX");
						String subject = parser.getAttributeValue(null,
								"Subject");
						String latitude = parser.getAttributeValue(null,
								"Latitude");
						String longtitude = parser.getAttributeValue(null,
								"Longtitude");
						String urllink = parser.getAttributeValue(null, "URL");

						temp = new YouTubeSoruce(Integer.valueOf(idx), subject,
								Double.valueOf(latitude),
								Double.valueOf(longtitude), urllink);
					}
					break;

				case XmlPullParser.TEXT:

					break;

				case XmlPullParser.END_TAG:
					if (temp != null) {
						p.add(temp);
					}
					temp = null;
				}

				parserEvent = parser.next();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return p;
	}

	public String getData(String url) {
		String youtubeUrl = "http://gdata.youtube.com/feeds/api/videos?vq=" + url;
		
		String titleString = null;
		try {
			URL nURL = new URL(youtubeUrl);
			InputStream html = nURL.openStream();
			Source source = new Source(new InputStreamReader(html, "utf-8"));
			
			source.fullSequentialParse();

			Element title = source.getAllElements(HTMLElementName.TITLE).get(1);
			titleString= title.getTextExtractor().toString();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return titleString;
	}
}
